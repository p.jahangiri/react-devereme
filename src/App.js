import 'devextreme/dist/css/dx.common.css';

import React from 'react';
import AppRoutes from 'routes';
import { useScreenSizeClass } from 'utils/media-query';

export default function App() {
      const screenSizeClass = useScreenSizeClass();

      return (
            <div className={`app ${screenSizeClass}`}>
                  <AppRoutes />
            </div>
      );
}
