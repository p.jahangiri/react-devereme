import './lib/polyfills';
import 'assets/styles/index.css';
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';

import React from 'react';
import ReactDOM from 'react-dom/client';
import { QueryClient, QueryClientProvider } from 'react-query';
import { BrowserRouter } from 'react-router-dom';

import App from './App';

const queryClient = new QueryClient();

const rootElement = document.getElementById('root');
ReactDOM.createRoot(rootElement).render(
      <React.StrictMode>
            <QueryClientProvider client={queryClient}>
                  <BrowserRouter>
                        <App />
                  </BrowserRouter>
            </QueryClientProvider>
      </React.StrictMode>,
);
