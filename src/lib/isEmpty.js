export const isEmpty = (obj) => {
      if (obj === null || obj === '' || obj === undefined) return true;
      if (!Object.entries(obj).length) return true;
      return false;
};
