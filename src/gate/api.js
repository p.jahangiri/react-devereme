import axios from 'axios';
import { isEmpty } from 'lib/isEmpty';

const client = axios.create({
      baseURL: 'http://5.182.44.198:4848/api/v1/',
      json: true,
});

client.defaults.timeout = 20000;

const call = async (method, url, data = {}) => {
      const token = localStorage.getItem('token');

      const headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
      };

      if (token !== '') {
            // localStorage.setItem("token", token);
            headers.Authorization = `Bearer ${token}`;
      }

      const request = { headers, method, url };

      if (!isEmpty(data)) {
            if (method === 'get') {
                  request.params = data;
            } else {
                  request.data = data;
            }
      }

      try {
            const response = await client(request);

            return Promise.resolve(response.data);
      } catch (error) {
            var err = null;
            if (error.response) {
                  // The request was made and the server responded with a status code
                  // that falls out of the range of 2xx
                  err = error.response;
            } else if (error.request) {
                  // The request was made but no response was received
                  // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                  // http.ClientRequest in node.js
                  err = { message: error.request._response };
            } else {
                  // Something happened in setting up the request that triggered an Error
                  err = error;
            }
            return Promise.reject(err);
      }
};

const auth = {
      async sign(url, data, method = 'post') {
            return new Promise((resolve, reject) => {
                  client({
                        data,
                        method,
                        url,
                  })
                        .then((response) => resolve(response.data))
                        .catch((error) => {
                              var err = null;
                              if (error.response) {
                                    // The request was made and the server responded with a status code
                                    // that falls out of the range of 2xxsign
                                    // console.log('err res', error.response);
                                    err = error.response;
                              } else if (error.request) {
                                    // The request was made but no response was received
                                    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                                    // http.ClientRequest in node.js
                                    // console.log('err req', error.request._response);
                                    err = { message: error.request._response };
                              } else {
                                    // Something happened in setting up the request that triggered an Error
                                    // console.log('err', error);
                                    err = error;
                              }
                              return reject(err);
                        });
            });
      },

      async signOut() {
            // tokenHelper.clear();
      },
};

const file = async (url, data) => {
      try {
            const token = localStorage.getItem('token');

            client.interceptors.request.use(
                  (config) => {
                        config.headers['Content-Type'] = `multipart/form-data;`;
                        config.headers.Authorization = `Bearer ${token}`;
                        config.timeout = 30000;

                        return config;
                  },
                  (err) => Promise.reject(err),
            );

            const response = await client({
                  url,
                  data,
                  method: 'post',
            });

            return Promise.resolve(response.data);
      } catch (error) {
            return Promise.reject(error.response);
      }
};

export default {
      ...auth,

      delete: (url, guest, data) => call('delete', url, guest, data),
      get: (url, guest, data) => call('get', url, guest, data),
      patch: (url, guest, data) => call('patch', url, guest, data),
      post: (url, guest, data) => call('post', url, guest, data),
      put: (url, guest, data) => call('put', url, guest, data),
      file: (url, data) => file(url, data),
};
