import api from './api';

const get = {
      getEmployees: () => api.get('Employee'),
};

export default {
      ...get,
};
