import ArrayStore from 'devextreme/data/array_store';
import DataSource from 'devextreme/data/data_source';
import {
      Button,
      Column,
      DataGrid,
      Editing,
      Scrolling,
      SearchPanel,
      Selection,
} from 'devextreme-react/data-grid';
import { Form, SimpleItem } from 'devextreme-react/form';
import { LoadPanel } from 'devextreme-react/load-panel';
import { Popup, ToolbarItem } from 'devextreme-react/popup';
import { RequiredRule } from 'devextreme-react/validator';
import gate from 'gate';
import React, { useMemo, useReducer, useState } from 'react';
import { useQuery } from 'react-query';

const colCountByScreen = {
      lg: 2,
      md: 2,
      sm: 1,
      xs: 1,
};
const formRef = React.createRef(),
      getForm = () => {
            return formRef.current.instance;
      };
const initPopupState = {
      formData: {},
      popupVisible: false,
      popupMode: '',
};
function Home() {
      let key = (Math.random() * 1000000).toFixed(0);

      const [showEmployeeInfo, setShowEmployeeInfo] = useState(false);

      const [selectedRowData, setSelectedRowData] = useState({
            name: '',
            job: '',
      });
      const { data, isLoading, error, isError } = useQuery(['employees'], () =>
            gate.getEmployees(),
      );

      const customerStore = new ArrayStore({
                  data: data?.data,
                  key: data?.data.id,
                  onPush: function () {
                        gridSource.reload();
                  },
            }),
            gridSource = new DataSource({
                  store: customerStore,
            });
      const [{ formData, popupVisible, popupMode }, dispatchPopup] = useReducer(
            popupReducer,
            initPopupState,
      );

      const confirmBtnOptions = useMemo(() => {
            return {
                  text: 'Confirm',
                  type: 'success',
                  onClick: confirmClick,
            };
      }, [formData]);

      const cancelBtnOptions = useMemo(() => {
            return {
                  text: 'Cancel',
                  onClick: cancelClick,
            };
      }, []);

      function onToolbarPreparing(e) {
            let toolbarItems = e.toolbarOptions.items;

            // customize addRow toolbar button
            for (let i = 0; i < toolbarItems.length; i++) {
                  let item = toolbarItems[i];
                  if (item.name === 'addRowButton') {
                        item.options.onClick = addClick;
                        break;
                  }
            }
      }

      function editClick(e) {
            showPopup('Edit', { ...e.row.data });
      }

      function addClick(e) {
            showPopup('Add', {});
      }

      function confirmClick(e) {
            let result = getForm().validate();
            if (result.isValid) {
                  customerStore.push([{ type: 'insert', data: formData }]);
                  if (popupMode === 'Add')
                        customerStore.push([{ type: 'insert', data: formData, key: [key] }]);
                  else if (popupMode === 'Edit')
                        customerStore.push([
                              { type: 'update', data: formData, key: [formData.id] },
                        ]);
                  dispatchPopup({ type: 'hidePopup' });
            }
      }

      function onSelectionChanged({ selectedRowsData }) {
            const data = selectedRowsData[0];
            setShowEmployeeInfo(true);
            setSelectedRowData({
                  name: data.name,
                  job: data.orgUnitName,
            });
      }

      function cancelClick(e) {
            dispatchPopup({ type: 'hidePopup' });
      }

      function showPopup(popupMode, data) {
            console.log(data, popupMode);
            dispatchPopup({ type: 'initPopup', data, popupMode });
      }

      function onHiding() {
            dispatchPopup({ type: 'hidePopup' });
      }
      if (isLoading) {
            return (
                  <LoadPanel
                        shadingColor="rgba(0,0,0,0.4)"
                        position={{ of: window }}
                        visible={isLoading}
                        showIndicator={true}
                        shading={true}
                        showPane={true}
                        closeOnOutsideClick={false}
                  />
            );
      }
      if (isError) {
            return (
                  <div className="error">
                        Something went wrong ...
                        <br />
                        {error.message}
                  </div>
            );
      }
      return (
            <div className="dataGrid">
                  <DataGrid
                        width={'70%'}
                        height={'400px'}
                        showBorders={true}
                        dataSource={gridSource}
                        showRowLines={true}
                        // showColumnLines={false}
                        onToolbarPreparing={onToolbarPreparing}
                        repaintChangesOnly={true}
                        onSelectionChanged={onSelectionChanged}
                        rowAlternationEnabled={true}
                        rtlEnabled={true}
                  >
                        <Scrolling mode="infinite" />
                        <Selection mode="single" />
                        <SearchPanel visible={true} />
                        <Editing
                              allowUpdating={true}
                              allowAdding={true}
                              allowDeleting={true}
                              useIcons={true}
                              mode="popup"
                        />
                        <Column dataField="name" dataType="string" caption="name" />
                        <Column dataField="firstName" dataType="string" caption="last name" />
                        <Column dataField="orgUnitCode" dataType="string" caption="job" />
                        <Column dataField="orgUnitName" dataType="string" caption="duty" />
                        <Column type="buttons">
                              <Button name="edit" onClick={editClick} />
                              <Button name="delete" />
                        </Column>
                  </DataGrid>
                  {showEmployeeInfo && (
                        <h4 style={{ marginTop: '10px' }}>
                              {selectedRowData.name} {selectedRowData.job}
                        </h4>
                  )}
                  <Popup
                        title={popupMode}
                        closeOnOutsideClick={true}
                        visible={popupVisible}
                        onHiding={onHiding}
                  >
                        <ToolbarItem
                              widget="dxButton"
                              location="after"
                              toolbar="bottom"
                              options={confirmBtnOptions}
                        />
                        <ToolbarItem
                              widget="dxButton"
                              location="after"
                              toolbar="bottom"
                              options={cancelBtnOptions}
                        />
                        <Form
                              ref={formRef}
                              formData={formData}
                              labelLocation="top"
                              showColonAfterLabel={true}
                              colCountByScreen={colCountByScreen}
                        >
                              <SimpleItem dataField="name">
                                    <RequiredRule />
                              </SimpleItem>
                              <SimpleItem dataField="firstName">
                                    <RequiredRule />
                              </SimpleItem>
                              <SimpleItem dataField="orgUnitCode">
                                    <RequiredRule />
                              </SimpleItem>
                              <SimpleItem dataField="orgUnitName">
                                    <RequiredRule />
                              </SimpleItem>
                        </Form>
                  </Popup>
            </div>
      );
}

export default Home;

function popupReducer(state, action) {
      switch (action.type) {
            case 'initPopup':
                  return {
                        formData: action.data,
                        popupVisible: true,
                        popupMode: action.popupMode,
                  };
            case 'hidePopup':
                  return {
                        popupVisible: false,
                  };
            default:
                  break;
      }
}
