module.exports = {
      'env': {
            'browser': true,
            'es2021': true,
            'jest': true,
      },
      'extends': [
            'plugin:react/recommended',
            'plugin:prettier/recommended',
            'plugin:react-hooks/recommended',
      ],
      'parserOptions': {
            'ecmaFeatures': {
                  'jsx': true,
            },
            'ecmaVersion': 2020,
            'sourceType': 'module',
      },
      'plugins': ['react', 'simple-import-sort', 'import', 'unused-imports'],
      'rules': {
            'react-display-name': 'off',
            'react/no-unescaped-entities': 'off',
            'simple-import-sort/exports': 'error',
            'simple-import-sort/imports': 'error',
            'react/react-in-jsx-scope': 'off',
            'react/prop-types': 'off',
            'unused-imports/no-unused-imports': 2,
            'import/first': 'error',
            'import/newline-after-import': 'error',
            'import/no-duplicates': 'error',
            'react/react-in-jsx-scope': 'off',
            'prettier/prettier': ['error', { endOfLine: 'auto' }],
            'import/newline-after-import': ['warn', { 'count': 1 }],
      },
      settings: {
            react: {
                  version: 'detect',
            },
      },
};
